# Vending Machine
The repository has a branch for each stage (stage-1, stage-2, stage-3). Master has the latest code which should be similar to stage-3. For testing I would recommend cloning master branch, since I added Docker at the end ( developing on my environment was a bit faster for a small app without dependencies, considering the availalbe time ). I left the branches there so progression and what was modified between the steps can be visible easier.

### Installation

##### Docker

- ```docker build -t george/vendingmachine .```

##### Standalone
- ```npm install``` - or - ```yarn```

### Usage

##### Docker
First build the image: ```docker build -t george/vendingmachine .```
- API server: ```docker run -p 3000:3000 -d george/vendingmachine``` assuming nothing is currently running on port 3000
- CLI: ```docker exec -it CONTAINER_ID script /dev/null -c "node /usr/src/app/build/cli.js --pence=58"```
- Tests run automatically when building the container
##### Standalone
- API: ```node ./build/server.js```
- CLI: ```node /usr/src/app/build/cli.js --pence=58```
- Run tests: ```npm test```


### REST API Examples

##### Create new coin
```shell
curl -X POST \
  http://localhost:3000/coin \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/x-www-form-urlencoded' \
  -d 'denomination=24&count=10'
```

##### Delete coin
```shell
curl -X DELETE \
  http://localhost:3000/coin/24 \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/x-www-form-urlencoded'
```
  
##### Update coin
```shell
curl -X PUT \
  http://localhost:3000/coin/24 \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/x-www-form-urlencoded' \
  -d count=5
```
  
##### Get available coins
```shell
curl -X GET \
  http://localhost:3000/coins \
  -H 'Cache-Control: no-cache'
```

##### Get change from pence
```shell
curl -X GET \
  http://localhost:3000/getChange/59 \
  -H 'Cache-Control: no-cache'
```