FROM node:carbon

WORKDIR /usr/src/app
COPY package*.json ./

RUN npm install
COPY . .
RUN npm test

EXPOSE 3000
CMD [ "node", "./build/server.js" ]