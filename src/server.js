import express from 'express';
import bodyParser from 'body-parser';
import CoinManager from './classes/CoinManager';
import VendingMachine from './classes/VendingMachine';

const app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const path = `${process.cwd()}/src/storage/coin-inventory.properties`;
const Manager = new CoinManager(path);

app.get('/getChange/:pence', (req, res) => {
    const VMachine = new VendingMachine(Manager.getCoins());
    try {
        res.json(VMachine.getChangeFor(req.params.pence, (coin) => {
            Manager.updateCoin(coin.getDenomination(), coin.getCount());
        }));
    } catch (ex) {
        res.json({error: 'Not enough coins available'});
    }
});

app.get('/coins', (req, res) => res.json(Manager.getCoins().filter(coin => coin.countNumber !== 0)));

app.get('/coin/:denomination', (req, res) => res.json({
    denomination: parseInt(req.params.denomination),
    count: Manager.getCoin(req.params.denomination)
}));

app.post('/coin', (req, res) => {
    if (!req.body.hasOwnProperty('denomination') || !req.body.hasOwnProperty('count')) {
        res.json({error: 'Missing fields.'});
    }

    const denomination = req.body.denomination;
    try {
        Manager.addCoin(denomination, parseInt(req.body.count));
        res.json({
            denomination: parseInt(denomination),
            count: Manager.getCoin(denomination)
        });
        Manager.apply();
    } catch(ex) {
        res.json({error: 'Denomination already exists.'});
    }
});

app.put('/coin/:denomination', (req, res) => {
    if (!req.body.hasOwnProperty('count')) {
        res.json({error: 'Missing fields.'});
    }

    const denomination = req.params.denomination;
    try {
        Manager.updateCoin(denomination, parseInt(req.body.count));
        res.json({
            denomination: parseInt(denomination),
            count: Manager.getCoin(denomination)
        });
        Manager.apply();
    } catch(ex) {
        res.json({error: 'Missing denomination.'});
    }
});

app.delete('/coin/:denomination', (req, res) => {
    const denomination = req.params.denomination;
    try {
        Manager.removeCoin(denomination);
        res.json({success: 'Coin removed.'});
        Manager.apply();
    } catch(ex) {
        res.json({error: 'Missing denomination.'});
    }
});

const server = app.listen(3000, () => {
    console.log("app running on port.", server.address().port);
});