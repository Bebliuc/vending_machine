import VendingMachine from './classes/VendingMachine';
import { argv } from 'yargs';
import CoinManager from './classes/CoinManager';

if (!argv.hasOwnProperty('pence') || typeof argv.pence !== 'number') {
    console.warn(`Invalid pence amount. Usage: --pence=59`);
    process.exit();
}

const path = `${process.cwd()}/src/storage/coin-inventory.properties`;
const Manager = new CoinManager(path);
const VMachine = new VendingMachine(Manager.getCoins());

const result = VMachine.getChangeFor(argv.pence, (coin, count) => {
    Manager.updateCoin(coin.getDenomination(), coin.getCount());
});

Manager.apply();

console.log(`Request change for ${argv.pence}`);

result.map(coin => {
    console.log(`${coin.count} coins x ${coin.denomination}p`);
});
