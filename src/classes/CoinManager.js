import reader from 'properties-reader';
import Coin from './Coin';
import fs from 'fs';

export default class CoinManager {

    constructor(path) {
        this.coins = [];
        this.path = path;

        try {
            this.db = reader(this.path);
            this.updateCoins();
        } catch (ex) {
            throw new Error(`File does not exist. ${this.path}`);
        }
    }

    updateCoins() {
        this.coins = [];
        Object.keys(this.db.path()).map(index => {
            this.coins.push(new Coin(parseInt(index), parseInt(this.db.path()[index])));
        });
    }

    addCoin(denomination, quantity) {
        const
            key = denomination.toString(),
            coin = this.db.get(key);
        if (!coin) {
           this.db.set(key, quantity);
           this.updateCoins();
        } else {
            throw new Error('Denomination already exists.');
        }
    }

    updateCoin(denomination, quantity) {
        const
            key = denomination.toString(),
            coin = this.db.get(key);

        if (coin) {
            this.db.set(key, quantity);
            this.updateCoins();
        } else {
            throw new Error('Denomination does not exists.');
        }
    }

    getCoin(denomination) {
        const
            key = denomination.toString(),
            coin = this.db.get(key);

        if (coin) {
            return coin;
        } else {
            throw new Error('Denomination does not exists.');
        }
    }

    removeCoin(denomination) {
        const
            key = denomination.toString(),
            coin = this.db.get(key);

        if (coin) {
            this.db.set(key, 0);
            this.updateCoins();
        } else {
            throw new Error('Denomination does not exists.');
        }
    }

    apply() {
        let saved = [];
        this.db.each((key, val) => saved.push(`${key}=${val}`));
        fs.writeFile(this.path, saved.reverse().join('\n'), (err) => {
            if (err) {
                throw new Error(err);
            }
        });
    }

    getPath = () => this.path;
    getCoins = () => this.coins;
}