export default class Coin {

    constructor(denomination, countNumber) {

        if (denomination < 1) {
            throw new Error('Invalid Denomination.');
        }

        if (typeof denomination !== 'number' || typeof countNumber !== 'number') {
            throw new TypeError(`Invalid Denomination ( ${denomination} ) or Count ( ${countNumber} ) type.`);
        }

        this.denomination = denomination;
        this.countNumber = countNumber;
    }

    use = (count) => {
        if (!this.countNumber && !this.countNumber < count) {
            throw Error('Not enough coins')
        }
        this.countNumber = this.countNumber - count;
    };

    getDenomination = () => this.denomination;
    getCount = () => this.countNumber;
}
