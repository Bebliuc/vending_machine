export default class VendingMachine {

    constructor(coins) {
        this.coins = coins;
        this.sortCoins();
    }

    sortCoins() {
        this.coins = this.coins.sort((a, b) => b.getDenomination() - a.getDenomination());
    }

    getChangeFor(pence, callback) {
        let values = [],
            coin,
            index = 0;

        if (pence === 0) {
            throw new Error('Change needs to be > 0');
        }

        const totalCoins = this.coins
            .map(coin => coin.getDenomination() * coin.getCount())
            .reduce((a, b) => a + b);

        if (pence > totalCoins) {
            throw new Error(`Not enough coins available. Maximum available: ${totalCoins}`);
        }

        while (pence) {
            coin = this.coins[index++];
            if (coin.getCount() > 0) {
                if (coin.getDenomination() <= pence) {
                    const count = Math.min(coin.getCount(), (pence - (pence % coin.getDenomination())) / coin.getDenomination());

                    values.push({
                        denomination: coin.getDenomination(),
                        count: count
                    });

                    coin.use(count);
                    pence = pence - (coin.getDenomination() * count);
                    if (callback) callback(coin, count);
                }
            }
        }

        return values;
    }

    getCoins = () => this.coins;
}
