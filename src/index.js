import VendingMachine from './classes/VendingMachine';
import CoinManager from './classes/CoinManager';

const path = `${process.cwd()}/src/storage/coin-inventory.properties`;
const Manager = new CoinManager(path);
const VMachine = new VendingMachine(Manager.getCoins());

console.log(VMachine.getChangeFor(3375));
