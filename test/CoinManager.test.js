import assert from 'power-assert';
import mock from 'mock-fs';

import CoinManager from '../src/classes/CoinManager';

describe('Coin Manager', () => {
    mock({
        'storage/coin-inventory.properties': `5=10
        10=20
        20=5
        1=40`
    });

    const path = 'storage/coin-inventory.properties';

    const Manager = new CoinManager(path);

    it('reads correct values from file', () => {
        assert.equal(Manager.getCoin(10), 20);
        assert.equal(Manager.getCoin(20), 5);
        assert.equal(Manager.getCoin(5), 10);
        assert.equal(Manager.getCoin(1), 40);
    });

    it('writes correct values to file', () => {
        Manager.addCoin(12, 5);
        assert.equal(Manager.getCoin(12), 5);
    });

    it('throws error when path is invalid', () => {
        assert.throws(() => { new CoinManager('/invalid/path'); }, Error);
    });

    it('throws error when adding existing denomination', () => {
        assert.throws(() => { Manager.addCoin(10, 5) }, Error);
    });

    it('throws error when updating missing denomination', () => {
        assert.throws(() => { Manager.updateCoin(11, 5) }, Error);
    });

    mock.restore();
});
