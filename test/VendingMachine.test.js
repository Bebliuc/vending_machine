import assert from 'power-assert';
import Coin from '../src/classes/Coin';
import VendingMachine from '../src/classes/VendingMachine';

describe('Vending Machine', () => {

    const coins = [
        new Coin(100, Number.MAX_SAFE_INTEGER),
        new Coin(50, Number.MAX_SAFE_INTEGER),
        new Coin(20, Number.MAX_SAFE_INTEGER),
        new Coin(10, Number.MAX_SAFE_INTEGER),
        new Coin(5, Number.MAX_SAFE_INTEGER),
        new Coin(2, Number.MAX_SAFE_INTEGER),
        new Coin(1, Number.MAX_SAFE_INTEGER)
    ];

    const VMachine = new VendingMachine(coins);

    it('registers correct coins', () => {
        assert.deepEqual(VMachine.getCoins(), coins);
    });

    it('calculates the right amount of pences', () => {

        assert.throws(() => VMachine.getChangeFor(0), Error);

        assert.deepEqual(VMachine.getChangeFor(11), [
            {
                denomination: 10,
                count: 1,
            }, {
                denomination: 1,
                count: 1
            }
        ]);

        assert.deepEqual(VMachine.getChangeFor(20), [
            {
                denomination: 20,
                count: 1,
            }
        ]);

        assert.deepEqual(VMachine.getChangeFor(98), [
            {
                denomination: 50,
                count: 1,
            },
            {
                denomination: 20,
                count: 2,
            },
            {
                denomination: 5,
                count: 1,
            },
            {
                denomination: 2,
                count: 1,
            },
            {
                denomination: 1,
                count: 1,
            }
        ]);

        assert.deepEqual(VMachine.getChangeFor(33), [
            {
                denomination: 20,
                count: 1,
            },
            {
                denomination: 10,
                count: 1,
            },
            {
                denomination: 2,
                count: 1,
            },
            {
                denomination: 1,
                count: 1,
            }
        ]);
    });

});