import assert from 'power-assert';
import Coin from '../src/classes/Coin';

describe('Coin', () => {

    const Pound = new Coin(100, 50);

    it('registers correct denomination', () => {
        assert.equal(Pound.getDenomination(), 100);
    });

    it('registers correct count', () => {
        assert.equal(Pound.getCount(), 50);
    });

    it('registers usage', () => {
        Pound.use(1);
        assert.equal(Pound.getCount(), 49);

        Pound.use(5);
        assert.equal(Pound.getCount(), 44);
    });

    it('accepts only number parameters', () => {
        assert.throws(() => new Coin(0), Error);
        assert.throws(() => new Coin('1', 2), TypeError);
        assert.throws(() => new Coin(1, '2'), TypeError);
        assert.throws(() => new Coin(1, false), TypeError);
        assert.throws(() => new Coin(false, 1), Error);
    });

});
